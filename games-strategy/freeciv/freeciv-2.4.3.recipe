SUMMARY="A free Civilization-like game."
DESCRIPTION="
Freeciv is a free Civilization-like game, primarily \
for X under Unix.  It has support for multiplayer games locally or \
over a network, and an AI which gives most people a run for their money.
Freeciv aims to be mostly rule-compatible with Civilization II [tm], \
published by Sid Meier and Microprose [tm].  A few rules are different \
where we think it makes more sense, and we have lots and lots of \
adjustable parameters to make customizing games possible.
Freeciv has been implemented completely independently of Civilization; \
you do not need to own Civilization to play Freeciv."
HOMEPAGE="http://www.freeciv.org"
COPYRIGHT="2002-2014 The Freeciv Team"
LICENSE="GNU GPL v2"
SRC_URI="http://sourceforge.net/projects/freeciv/files/Freeciv%202.4/$portVersion/freeciv-$portVersion.tar.bz2"
CHECKSUM_SHA256="7f107fe3b09f37934410dc7a64ac1b99a95997ddf53da53933b75d4da79fa899"
REVISION="1"
ARCHITECTURES="x86 x86_64"
if [ $effectiveTargetArchitecture != x86_gcc2 ]; then
	# x86_gcc2 is fine as primary target architecture as long as we're building
	# for a different secondary architecture.
	ARCHITECTURES="$ARCHITECTURES x86_gcc2"
else
	ARCHITECTURES="$ARCHITECTURES !x86_gcc2"
fi
SECONDARY_ARCHITECTURES="x86"

PROVIDES="
	freeciv$secondaryArchSuffix = $portVersion
	app:freeciv$secondaryArchSuffix = $portVersion
	cmd:freeciv_manual$secondaryArchSuffix
	cmd:freeciv_sdl$secondaryArchSuffix
	cmd:freeciv_server$secondaryArchSuffix
	"

REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libbz2$secondaryArchSuffix
	lib:libcurl$secondaryArchSuffix
	lib:libfreetype$secondaryArchSuffix
	lib:libiconv$secondaryArchSuffix
	lib:liblzma$secondaryArchSuffix
	lib:libpng$secondaryArchSuffix
	lib:libreadline$secondaryArchSuffix
	lib:libsdl$secondaryArchSuffix
	lib:libSDL_image$secondaryArchSuffix
	lib:libSDL_mixer$secondaryArchSuffix
	lib:libz$secondaryArchSuffix
	
	lib:libSDL_image_1.2$secondaryArchSuffix
	lib:libSDL_mixer_1.2$secondaryArchSuffix
	lib:libcrypto$secondaryArchSuffix
	lib:libssl$secondaryArchSuffix
	lib:libintl$secondaryArchSuffix
	lib:libjpeg$secondaryArchSuffix
	lib:libGL$secondaryArchSuffix
	"
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	devel:libbz2$secondaryArchSuffix
	devel:libcurl$secondaryArchSuffix
	devel:libfreetype$secondaryArchSuffix
	devel:libiconv$secondaryArchSuffix
	devel:liblzma$secondaryArchSuffix
	devel:libpng$secondaryArchSuffix
	devel:libreadline$secondaryArchSuffix
	devel:libsdl$secondaryArchSuffix
	devel:libSDL_image$secondaryArchSuffix
	devel:libSDL_mixer$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
	
	devel:libcrypto$secondaryArchSuffix
	devel:libssl$secondaryArchSuffix
	devel:libjpeg$secondaryArchSuffix
	"
BUILD_PREREQUIRES="
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:make
	cmd:autoconf
	cmd:aclocal
	cmd:libtool
	cmd:gettext
	cmd:find
	cmd:pkg_config$secondaryArchSuffix
	"

BUILD()
{
	autoreconf -fi
	runConfigure ./configure
	make $jobArgs
}

INSTALL()
{
	make install
	
	mkdir -p $appsDir/freeciv
	mv $binDir/freeciv-sdl $appsDir/freeciv/freeciv
	addAppDeskbarSymlink $appsDir/freeciv/freeciv
	
	rm -rf $prefix/lib
}
