SUMMARY="A lightweight XPS and PDF rendering library."
DESCRIPTION="
The renderer in MuPDF is tailored for high quality anti-aliased graphics. It \
renders text with metrics and spacing accurate to within fractions of a pixel \
for the highest fidelity in reproducing the look of a printed page on screen.

MuPDF is also small, fast, and yet complete. It supports PDF 1.7 with \
transparency, encryption, hyperlinks, annotations, searching and more. It also \
reads XPS and OpenXPS documents. MuPDF is written modularly, so features can \
be added on by integrators if they so desire.
"
HOMEPAGE="http://www.mupdf.com/"
LICENSE="AGPL-3.0"
COPYRIGHT="2006-2013 Artifex Software, Inc"
SRC_URI="http://www.mupdf.com/downloads/mupdf-$portVersion-source.tar.gz"
SOURCE_DIR="mupdf-$portVersion-source"
CHECKSUM_SHA256="9813ee330950a641364181649ebd369c947adbcea9d41ed953ec5365640ea563"
PATCHES="mupdf-1.6.patchset"

REVISION="1"
ARCHITECTURES="x86 x86_64"
if [ $effectiveTargetArchitecture != x86_gcc2 ]; then
	# x86_gcc2 is fine as primary target architecture as long as we're building
	# for a different secondary architecture.
	ARCHITECTURES="$ARCHITECTURES x86_gcc2"
else
	ARCHITECTURES="$ARCHITECTURES !x86_gcc2"
fi
SECONDARY_ARCHITECTURES="x86"


PROVIDES="
	mupdf$secondaryArchSuffix = $portVersion compat >= 1.1
	devel:libmupdf$secondaryArchSuffix = $portVersion compat >= 1.1
	"

REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libjpeg$secondaryArchSuffix
	lib:libz$secondaryArchSuffix
	lib:libfreetype$secondaryArchSuffix
	openjpeg$secondaryArchSuffix
	lib:libjbig2dec$secondaryArchSuffix
	"

BUILD_REQUIRES="
	devel:libopenjp2$secondaryArchSuffix
	devel:libjbig2dec$secondaryArchSuffix
	devel:libjpeg$secondaryArchSuffix
	devel:libfreetype$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
	"

BUILD_PREREQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	cmd:find
	cmd:freetype_config$secondaryArchSuffix
	cmd:gcc$secondaryArchSuffix
	cmd:make
	cmd:pkg_config$secondaryArchSuffix
	"

BUILD()
{
	make $jobArgs libs
}

INSTALL()
{
	make install-libs \
		prefix=$prefix \
		bindir=$binDir \
		libdir=$libDir \
		incdir=$includeDir \
		mandir=$manDir

	prepareInstalledDevelLibs libmupdf
}
