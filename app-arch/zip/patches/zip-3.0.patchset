From 6041b90bd0edc53e7596d2e18119cde24987b314 Mon Sep 17 00:00:00 2001
From: Kacper Kasper <kacperkasper@gmail.com>
Date: Thu, 3 Apr 2014 20:58:47 +0000
Subject: Haiku support


diff --git a/beos/Makefile b/beos/Makefile
index 1b9e613..d66e6e3 100644
--- a/beos/Makefile
+++ b/beos/Makefile
@@ -22,16 +22,30 @@ BIND = $(CC)
 AS   = $(CC) -c
 CPP  = $(CC) -E
 
-INSTALL = install
+# probably can change this to 'install' if you have it
+INSTALL_PROGRAM = cp
+# probably can change this to 'install -d' if you have it
+# XXX NextStep 3.3 and Openstep 4.x don't know about -p !
+INSTALL_D = mkdir -p
+CHMOD = chmod
+BINFLAGS = 755
+MANFLAGS = 644
 
 # Target directories
-prefix = /boot/home/config
-BINDIR = $(prefix)/bin
-manext = 1
-MANDIR = $(prefix)/man/man$(manext)
+PREFIX = `finddir B_COMMON_DIRECTORY`
+BINDIR = $(PREFIX)/bin
+MANEXT = 1
+MANDIR = $(PREFIX)/documentation/man
+MANDIRF = $(MANDIR)/man$(MANEXT)
+INCLUDEDIR = `finddir B_SYSTEM_DEVELOP_DIRECTORY`/headers
+LIBDIR = `finddir B_SYSTEM_LIB_DIRECTORY`
 ZIPMANUAL = MANUAL
-
-VERSION = Version 2.3 of __DATE__
+ZIPMANUALcloak = zipcloak.txt
+ZIPMANUALnote = zipnote.txt
+ZIPMANUALsplit = zipsplit.txt
+ZIPMANUALs = zip.txt zipcloak.txt zipnote.txt zipsplit.txt
+PKGDIR = IZzip
+VERSION = Version 3.0
 
 ######################################################################
 # Things that change:
@@ -65,14 +79,14 @@ CC:=gcc
 
 # Removed -Wconversion and -Wshadow because of the unnecessary warnings 
 # they generate. - Sept. 28, 1999
-CFLAGS:=-O3 -mpentiumpro \
+CFLAGS:=-O3  \
 		-Wall -Wno-multichar -Wno-ctor-dtor-privacy \
 		-Wbad-function-cast -Woverloaded-virtual \
-		-I. -I/boot/develop/headers/be/support \
-	    -I/boot/develop/headers/be/storage \
+		-I. -I$(INCLUDEDIR)/be/support \
+	    -I$(INCLUDEDIR)/be/storage \
 	    -DHAVE_DIRENT_H -DPASSWD_FROM_STDIN # -DASMV
 LFLAGS1:=
-LFLAGS2:=-L/boot/develop/lib/x86 -lbe -lroot
+LFLAGS2:=-L$(LIBDIR) -lbe -lroot
 OBJA = #match.o
 TARGET=$(ZIPS)
 
@@ -88,44 +102,40 @@ all:
 ######################################################################
 # Object file lists and other build goodies
 
-# Object file lists
+# object file lists
 OBJZ = zip.o zipfile.o zipup.o fileio.o util.o globals.o crypt.o ttyio.o \
-       beos.o crc32.o
+       beos.o crc32.o zbz2err.o setfiletype.o
 OBJI = deflate.o trees.o
-# OBJA moved into ifeq block above; we'll use assembly for x86
-OBJU = zipfile_.o fileio_.o util_.o globals.o beos_.o
+OBJA =
+OCRCU8 =
+OCRCTB = crc32_.o
+OBJU = zipfile_.o fileio_.o util_.o globals.o beos_.o setfiletype.o $(OCRCU8)
 OBJN = zipnote.o  $(OBJU)
-OBJC = zipcloak.o $(OBJU) crc32_.o crypt_.o ttyio.o
+OBJC = zipcloak.o $(OBJU) $(OCRCTB) crypt_.o ttyio.o
 OBJS = zipsplit.o $(OBJU)
 
-# Headers
 ZIP_H = zip.h ziperr.h tailor.h beos/osdep.h
 
-# What to build?
-ZIPS = zip zipnote zipsplit zipcloak
-
 # suffix rules
 .SUFFIXES:
 .SUFFIXES: _.o .o .c .doc .1
 .c_.o:
-	rm -f $*_.c; $(LN) $< $*_.c
-	$(CC) -c $(CFLAGS) -DUTIL $*_.c
-	rm -f $*_.c
+	$(CC) -c $(CFLAGS) -DUTIL -o $@ $<
 
 .c.o:
 	$(CC) -c $(CFLAGS) $<
 
 .1.doc:
-	groff -man -Tascii $< > $@
+	nroff -man $< | col -bx | uniq > $@
 
-# rules for zip, zipnote, zipcloak, zipsplit, and the Zip MANUAL.
+# rules for zip, zipnote, zipcloak, zipsplit, and the Zip MANUALs.
 $(OBJZ): $(ZIP_H)
 $(OBJI): $(ZIP_H)
 $(OBJN): $(ZIP_H)
 $(OBJS): $(ZIP_H)
 $(OBJC): $(ZIP_H)
-zip.o crc32.o crypt.o fileio.o zipfile.o zipup.o: crc32.h
-zipcloak.o crc32_.o crypt_.o fileio_.o zipfile_.o: crc32.h
+zip.o zipup.o zipfile.o fileio.o crc32.o crypt.o: crc32.h
+zipcloak.o zipfile_.o fileio_.o crc32_.o crypt_.o: crc32.h
 zip.o zipup.o crypt.o ttyio.o zipcloak.o crypt_.o: crypt.h
 zip.o zipup.o zipnote.o zipcloak.o zipsplit.o: revision.h
 zip.o crypt.o ttyio.o zipcloak.o crypt_.o: ttyio.h
@@ -134,42 +144,69 @@ zipup.o: beos/zipup.h
 match.o: match.S
 	$(CPP) match.S > _match.s
 	$(AS) _match.s
-	mv -f _match.o match.o
+	mv _match.o match.o
 	rm -f _match.s
 
+crc_i386.o: crc_i386.S
+	$(CPP) crc_i386.S > _crc_i386.s
+	$(AS) _crc_i386.s
+	mv _crc_i386.o crc_i386.o
+	rm -f _crc_i386.s
+
 beos.o: beos/beos.c
 	$(CC) -c $(CFLAGS) beos/beos.c
 
 beos_.o: beos/beos.c
-	rm -f $*_.c; $(LN) beos/beos.c $*_.c
-	$(CC) -c $(CFLAGS) -DUTIL $*_.c
-	rm -f $*_.c
+	$(CC) -c $(CFLAGS) -DUTIL -o $@ beos/beos.c
+	
+setfiletype.o: beos/setfiletype.c
+	$(CC) -c $(CFLAGS) -o $@ beos/setfiletype.c
+
+ZIPS = zip$E zipcloak$E zipnote$E zipsplit$E
 
 zips: $(ZIPS)
-zipsman: $(ZIPS) $(ZIPMANUAL)
+zipsman: $(ZIPS) $(ZIPMANUALs)
 
-zip: $(OBJZ) $(OBJI) $(OBJA)
-	$(BIND) -o zip $(LFLAGS1) $(OBJZ) $(OBJI) $(OBJA) $(LFLAGS2)
-zipnote: $(OBJN)
-	$(BIND) -o zipnote $(LFLAGS1) $(OBJN) $(LFLAGS2)
-zipcloak: $(OBJC)
-	$(BIND) -o zipcloak $(LFLAGS1) $(OBJC) $(LFLAGS2)
-zipsplit: $(OBJS)
-	$(BIND) -o zipsplit $(LFLAGS1) $(OBJS) $(LFLAGS2)
+zip$E: $(OBJZ) $(OBJI) $(OBJA) $(LIB_BZ)
+	$(BIND) -o zip$E $(LFLAGS1) $(OBJZ) $(OBJI) $(OBJA) $(LFLAGS2)
+zipnote$E: $(OBJN)
+	$(BIND) -o zipnote$E $(LFLAGS1) $(OBJN) $(LFLAGS2)
+zipcloak$E: $(OBJC) $(OCRCTB)
+	$(BIND) -o zipcloak$E $(LFLAGS1) $(OBJC) $(LFLAGS2)
+zipsplit$E: $(OBJS)
+	$(BIND) -o zipsplit$E $(LFLAGS1) $(OBJS) $(LFLAGS2)
 
 $(ZIPMANUAL): man/zip.1
-	groff -man -Tascii man/zip.1 > $(ZIPMANUAL)
+	nroff -man man/zip.1 | col -bx | uniq > $(ZIPMANUAL)
+
+$(ZIPMANUALcloak): man/zipcloak.1
+	nroff -man man/zipcloak.1 | col -bx | uniq > $(ZIPMANUALcloak)
 
+$(ZIPMANUALnote): man/zipnote.1
+	nroff -man man/zipnote.1 | col -bx | uniq > $(ZIPMANUALnote)
+
+$(ZIPMANUALsplit): man/zipsplit.1
+	nroff -man man/zipsplit.1 | col -bx | uniq > $(ZIPMANUALsplit)
+	
 # install
 install:        $(ZIPS)
-	$(INSTALL) -m755 $(ZIPS) $(BINDIR)
-	mkdir -p $(MANDIR)
-	$(INSTALL) -m644 man/zip.1 $(MANDIR)/zip.$(manext)
+	-$(INSTALL_D) $(BINDIR)
+	$(INSTALL_PROGRAM) $(ZIPS) $(BINDIR)
+	-cd $(BINDIR); $(CHMOD) $(BINFLAGS) $(ZIPS)
+	-$(INSTALL_D) $(MANDIRF)
+	$(INSTALL_PROGRAM) man/zip.1 $(MANDIRF)/zip.$(MANEXT)
+	$(CHMOD) $(MANFLAGS) $(MANDIRF)/zip.$(MANEXT)
+	$(INSTALL_PROGRAM) man/zipcloak.1 $(MANDIRF)/zipcloak.$(MANEXT)
+	$(CHMOD) $(MANFLAGS) $(MANDIRF)/zipcloak.$(MANEXT)
+	$(INSTALL_PROGRAM) man/zipnote.1 $(MANDIRF)/zipnote.$(MANEXT)
+	$(CHMOD) $(MANFLAGS) $(MANDIRF)/zipnote.$(MANEXT)
+	$(INSTALL_PROGRAM) man/zipsplit.1 $(MANDIRF)/zipsplit.$(MANEXT)
+	$(CHMOD) $(MANFLAGS) $(MANDIRF)/zipsplit.$(MANEXT)
 
 uninstall:
 	-cd $(BINDIR); rm -f $(ZIPS)
-	-cd $(MANDIR); rm -f zip.$(manext)
-
+	-cd $(MANDIRF); rm -f \
+	 zip.$(MANEXT) zipcloak.$(MANEXT) zipnote.$(MANEXT) zipsplit.$(MANEXT)
 dist: $(ZIPMANUAL)
 	zip -u9T zip`sed -e '/VERSION/!d' -e 's/.*"\(.*\)".*/\1/' \
 			  -e s/[.]//g -e q revision.h` \
diff --git a/beos/beos.c b/beos/beos.c
index d8d16df..d8df5d5 100644
--- a/beos/beos.c
+++ b/beos/beos.c
@@ -31,7 +31,7 @@
 
 #include <kernel/fs_attr.h>
 #include <storage/Mime.h>
-#include <support/byteorder.h>
+#include <support/ByteOrder.h>
 
 
 #define PAD 0
@@ -851,32 +851,6 @@ int set_extra_field(z, z_utim)
     return ZE_OK;
 }
 
-/* ---------------------------------------------------------------------- */
-/* Set a file's MIME type.                                                */
-void setfiletype( const char *file, const char *type )
-{
-    int fd;
-    attr_info fa;
-    ssize_t wrote_bytes;
-
-    fd = open( file, O_RDWR );
-    if( fd < 0 ) {
-        zipwarn( "can't open zipfile to write file type", "" );
-        return;
-    }
-
-    fa.type = B_MIME_STRING_TYPE;
-    fa.size = (off_t)(strlen( type ) + 1);
-
-    wrote_bytes = fs_write_attr( fd, BE_FILE_TYPE_NAME, fa.type, 0,
-                                 type, fa.size );
-    if( wrote_bytes != (ssize_t)fa.size ) {
-        zipwarn( "couldn't write complete file type", "" );
-    }
-
-    close( fd );
-}
-
 int deletedir(d)
 char *d;                /* directory to delete */
 /* Delete the directory *d if it is empty, do nothing otherwise.
diff --git a/beos/setfiletype.c b/beos/setfiletype.c
new file mode 100644
index 0000000..cd8dd2e
--- /dev/null
+++ b/beos/setfiletype.c
@@ -0,0 +1,33 @@
+#include <fcntl.h>
+#include <string.h>
+
+#include <kernel/fs_attr.h>
+#include <storage/Mime.h>
+
+#include "beos/osdep.h"
+
+/* ---------------------------------------------------------------------- */
+/* Set a file's MIME type.                                                */
+void setfiletype( const char *file, const char *type )
+{
+    int fd;
+    attr_info fa;
+    ssize_t wrote_bytes;
+
+    fd = open( file, O_RDWR );
+    if( fd < 0 ) {
+        zipwarn( "can't open zipfile to write file type", "" );
+        return;
+    }
+
+    fa.type = B_MIME_STRING_TYPE;
+    fa.size = (off_t)(strlen( type ) + 1);
+
+    wrote_bytes = fs_write_attr( fd, BE_FILE_TYPE_NAME, fa.type, 0,
+                                 type, fa.size );
+    if( wrote_bytes != (ssize_t)fa.size ) {
+        zipwarn( "couldn't write complete file type", "" );
+    }
+
+    close( fd );
+}
diff --git a/tailor.h b/tailor.h
index 272f979..579609a 100644
--- a/tailor.h
+++ b/tailor.h
@@ -52,7 +52,7 @@
 #include "atheos/osdep.h"
 #endif
 
-#ifdef __BEOS__
+#if (defined(__BEOS__) || defined(__HAIKU__))
 #include "beos/osdep.h"
 #endif
 
diff --git a/ttyio.c b/ttyio.c
index 5899fdc..15cc835 100644
--- a/ttyio.c
+++ b/ttyio.c
@@ -52,7 +52,7 @@
 #  define GLOBAL(g) G.g
 #endif
 
-#if (defined(__ATHEOS__) || defined(__BEOS__))  /* why yes, we do */
+#if (defined(__ATHEOS__) || defined(__BEOS__) || defined(__HAIKU__))  /* why yes, we do */
 #  define HAVE_TERMIOS_H
 #endif
 
diff --git a/ttyio.h b/ttyio.h
index df6a4ed..5180af1 100644
--- a/ttyio.h
+++ b/ttyio.h
@@ -60,7 +60,7 @@
 #  endif
 #endif
 
-#if (defined(__ATHEOS__) || defined(__BEOS__) || defined(UNIX))
+#if (defined(__ATHEOS__) || defined(__BEOS__) || defined(__HAIKU__) || defined(UNIX))
 #  ifndef ATH_BEO_UNX
 #    define ATH_BEO_UNX
 #  endif
diff --git a/zip.c b/zip.c
index 439821f..173d922 100644
--- a/zip.c
+++ b/zip.c
@@ -1845,7 +1845,7 @@ int rename_split(temp_name, out_path)
 int set_filetype(out_path)
   char *out_path;
 {
-#ifdef __BEOS__
+#if (defined(__BEOS__) || defined(__HAIKU__))
   /* Set the filetype of the zipfile to "application/zip" */
   setfiletype( out_path, "application/zip" );
 #endif
diff --git a/zip.h b/zip.h
index ba03160..208a0c7 100644
--- a/zip.h
+++ b/zip.h
@@ -78,12 +78,6 @@ freely, subject to the above disclaimer and the following restrictions:
 
 #define ZIP   /* for crypt.c:  include zip password functions, not unzip */
 
-/* Types centralized here for easy modification */
-#define local static            /* More meaningful outside functions */
-typedef unsigned char uch;      /* unsigned 8-bit value */
-typedef unsigned short ush;     /* unsigned 16-bit value */
-typedef unsigned long ulg;      /* unsigned 32-bit value */
-
 /* Set up portability */
 #include "tailor.h"
 
@@ -129,6 +123,13 @@ typedef unsigned long ulg;      /* unsigned 32-bit value */
 #  define MATCH shmatch         /* Default for pattern matching: UNIX style */
 #endif
 
+/* Types centralized here for easy modification */
+#define local static            /* More meaningful outside functions */
+typedef unsigned char uch;      /* unsigned 8-bit value */
+typedef unsigned short ush;     /* unsigned 16-bit value */
+typedef unsigned long ulg;      /* unsigned 32-bit value */
+
+
 /* Structure carrying extended timestamp information */
 typedef struct iztimes {
    time_t atime;                /* new access time */
diff --git a/zipcloak.c b/zipcloak.c
index 37bd414..f3afc36 100644
--- a/zipcloak.c
+++ b/zipcloak.c
@@ -54,7 +54,7 @@ ZCONST uLongf *crc_32_tab;
 int set_filetype(out_path)
   char *out_path;
 {
-#ifdef __BEOS__
+#if (defined(__BEOS__) || defined(__HAIKU__))
   /* Set the filetype of the zipfile to "application/zip" */
   setfiletype( out_path, "application/zip" );
 #endif
diff --git a/zipnote.c b/zipnote.c
index 5e02cb6..315a3de 100644
--- a/zipnote.c
+++ b/zipnote.c
@@ -72,7 +72,7 @@ void zipnotewarn(ZCONST char *a, ZCONST char *b);
 int set_filetype(out_path)
   char *out_path;
 {
-#ifdef __BEOS__
+#if (defined(__BEOS__) || defined(__HAIKU__))
   /* Set the filetype of the zipfile to "application/zip" */
   setfiletype( out_path, "application/zip" );
 #endif
diff --git a/zipsplit.c b/zipsplit.c
index 8db76a1..ecced0c 100644
--- a/zipsplit.c
+++ b/zipsplit.c
@@ -96,7 +96,7 @@ int talln = 0;          /* number of entries in talls[] */
 int set_filetype(out_path)
   char *out_path;
 {
-#ifdef __BEOS__
+#if (defined(__BEOS__) || defined(__HAIKU__))
   /* Set the filetype of the zipfile to "application/zip" */
   setfiletype( out_path, "application/zip" );
 #endif
diff --git a/zipup.c b/zipup.c
index 39f7d9c..80c3101 100644
--- a/zipup.c
+++ b/zipup.c
@@ -68,7 +68,7 @@
 #  include "atari/zipup.h"
 #endif
 
-#ifdef __BEOS__
+#if (defined(__BEOS__) || defined(__HAIKU__))
 #  include "beos/zipup.h"
 #endif
 
-- 
1.8.3.4

